﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RepoManager : MonoBehaviour {

    public GameObject Prefab;
    public string GithubApiUrl;
    public Vector3 InitialPosition;

    List<Repo> repos;

    //	List<GameObject> boats;
    public int MaxActiveBoats = 4;
    float TimeSinceLastBoatSpawn = 0f;
    public float BoatSpawnRate = 1.0f;

    Dictionary<string, Color> colors;
    string[] colorKeys;

    void Start()
    {
        repos = new List<Repo>();
        colors = new Dictionary<string, Color>();
        ParseColors();
        //		boats = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        // Make more boats
        if (!GameState.Paused)
        {
            if (TimeSinceLastBoatSpawn > BoatSpawnRate)
            {
                SpawnBoat();
                TimeSinceLastBoatSpawn = 0.0f;
            }
            else
            {
                TimeSinceLastBoatSpawn += Time.deltaTime;
            }
        }
    }

    protected void FetchNewRepositories()
    {
        string url = GithubApiUrl + "/repositories";
        WWW www = new WWW(url);
        StartCoroutine(ProcessRepositoryData(www));
    }

    protected void ParseColors()
    {
        TextAsset fileData = (TextAsset) Resources.Load("Colors");
        JSONObject data = new JSONObject(fileData.text);
        foreach (string key in data.keys)
        {
            Color color;
            string hex = data.GetField(key).str;
            ColorUtility.TryParseHtmlString(hex, out color);
            colors.Add(key, color);
        }
        colorKeys = new string[colors.Keys.Count];
        colors.Keys.CopyTo(colorKeys, 0);
    }

    protected IEnumerator ProcessRepositoryData(WWW data)
    {
        yield return data;

        repos.Clear();
        JSONObject jRepos = new JSONObject(data.text);
        for (int i = 0; i < jRepos.list.Count; i++) {
            JSONObject jRepo = jRepos.list[i];
            string json = jRepo.ToString();
            Repo repo = Repo.CreateFromJSON(json);
            repos.Add(repo);
        }
    }

    protected void SpawnBoat()
    {
        GameObject boat = (GameObject) Instantiate(Prefab, InitialPosition, Quaternion.identity);
        boat.SendMessage("SetMoveSpeed", Random.Range(1, 3));
        boat.transform.SetParent(this.transform);

        // Change the color of the right sail to that of
        // a particular language from github
        SpriteRenderer sr = GetRightSail(boat);
        int k = Random.Range(0, colorKeys.Length);
        string name = colorKeys[k];
        sr.color = colors[name];

        // Change the name of the boat to the selected language
        boat.name = name;
    }

    public SpriteRenderer GetRightSail(GameObject boat)
    {
        foreach (SpriteRenderer sr in boat.GetComponentsInChildren<SpriteRenderer>())
        {
            if (sr.gameObject.name == "right sail")
            {
                return sr;
            }
        }
        return null;;
    }

    public void DestroyBoats()
    {
    	foreach (Transform child in GetComponentsInChildren<Transform>())
    	{
    		if (child.gameObject.name != this.name)
    		{
	    		Destroy(child.gameObject);
    		}
    	}
    }

}
