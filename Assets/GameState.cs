﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameState {

    public static bool Paused     = true;
    public static bool InInfoMenu = false;
    public static int Score       = 0;
    public static int HighScore   = 0;

    public static void MainMenuToInfoMenu()
    {
        GameState.InInfoMenu = true;
    }

    public static void InfoMenuToMainMenu()
    {
        GameState.InInfoMenu = false;
    }

    public static void StartGame()
    {
        GameState.Paused = false;
        GameState.InInfoMenu = false;
    }

    public static void UnPause()
    {
        Paused = false;
    }
}
