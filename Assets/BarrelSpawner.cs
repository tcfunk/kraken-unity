﻿using UnityEngine;
using System.Collections;

public class BarrelSpawner : MonoBehaviour {


    public GameObject Prefab;
    public Vector3 InitialPosition;

    //	List<GameObject> boats;
    public int MaxActive = 1;
    float TimeSinceLastSpawn = 0f;
    public float SpawnRate = 10.0f;

    string[] colorKeys;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // Make more boats
        if (!GameState.Paused)
        {
            if (TimeSinceLastSpawn > SpawnRate)
            {
                SpawnBarrel();
                TimeSinceLastSpawn = 0.0f;
            }
            else
            {
                TimeSinceLastSpawn += Time.deltaTime;
            }
        }
    }


    protected void SpawnBarrel()
    {
        GameObject barrel = (GameObject) Instantiate(Prefab, InitialPosition, Quaternion.identity);
        barrel.SendMessage("SetMoveSpeed", Random.Range(1, 3));
        barrel.transform.SetParent(this.transform);
    }

    public void DestroyBarrels()
    {
    	foreach (Transform child in GetComponentsInChildren<Transform>())
    	{
    		if (child.gameObject.name != this.name)
    		{
	    		Destroy(child.gameObject);
    		}
    	}
	}
}
