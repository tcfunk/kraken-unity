﻿using UnityEngine;
using System.Collections;

public class Barrel : MonoBehaviour {

    public float waveFreq = 1.0f;
    public float moveSpeed = 10.0f;

    public GameObject Explosion;

    protected float baseY;
    protected float age;
    protected SpriteRenderer sprite;

    // Camera info
    Bounds orthoBounds;

    GameObject Kraken;

    bool isDead = false;

    // Use this for initialization
    void Start () {
        float x = Camera.main.orthographicSize;
        float y = x / Camera.main.aspect;
        orthoBounds = new Bounds(Vector3.zero, new Vector3(x*4, y*4, 0f));

        age = 0f;
        baseY = transform.position.y;
        sprite = (SpriteRenderer)GetComponent<SpriteRenderer>();

        Kraken = GameObject.Find("Kraken");
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameState.Paused)
        {
            UpdateAge();
            UpdatePosition();
            UpdateRotation();
            RemoveIfOffStage();
        }
    }

    protected void UpdateAge() {
        age += Time.deltaTime;
    }

    protected void UpdatePosition() {
        if (!isDead)
        {
            float dy = Mathf.Cos(age*Mathf.PI)*0.25f;
            Vector3 pos = transform.position;
            pos.y = baseY + dy;

            pos.x += moveSpeed * Time.deltaTime;
            transform.position = pos;
        }
    }

    protected void UpdateRotation() {

    }

    protected void RemoveIfOffStage() {
        Vector3 pos = sprite.bounds.min;
        if (pos.x > orthoBounds.max.x || pos.y < orthoBounds.min.y) {
            Destroy(this.gameObject);
        }
    }

    public void SetMoveSpeed(float speed) {
        moveSpeed = speed;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
    	Instantiate(Explosion, transform.position, transform.rotation);
    	Destroy(gameObject);
    	Kraken.SendMessage("DestroyTentacle", collision.collider.gameObject);
    }
}
