﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	public float LifeTime = 1.0f;

	float TimeAlive = 0f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (TimeAlive > LifeTime)
		{
			Destroy(gameObject);
		}
		TimeAlive += Time.deltaTime;
		transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, TimeAlive/LifeTime);
	}

}
