﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class TentacleSpawner : MonoBehaviour {

    public GameObject[] BasePositions;
    public GameObject TentacleSection;
    bool[] isAlive;

    Vector3 SquidStartPosition;
    public Vector3 SquidEndPosition;
    public float SquidFadeInTime = 3f;

    public int NumberOfSections  = 10;
    public float MidSectionMass  = 10f;
    public float LastSectionMass = 20f;
    public float TentacleMoveSpeed = 1f;

    public GameObject PauseMenu;
    public UnityEngine.UI.Text ScoreUI;

    public GameObject BarrelContainer;
    public GameObject BoatContainer;

    protected float TimeAlive;
    protected bool inPosition       = false;
    protected bool isGrabbing       = false;
    protected bool tentaclesSpawned = false;

    protected int score = 0;

    protected GameObject[] EndSections;

    // Camera info
    Bounds orthoBounds;

    // Use this for initialization
    void Start ()
    {
	    SquidStartPosition = transform.position;
        float x = Camera.main.orthographicSize;
        float y = x / Camera.main.aspect;
        orthoBounds = new Bounds(Vector3.zero, new Vector3(x*2, y*2, 0f));
        Reset();
    }

    // Update is called once per frame
    void Update ()
    {
        if (!GameState.Paused && tentaclesSpawned)
        {
            TimeAlive += Time.deltaTime;
            if (inPosition)
            {
                HandleInput();
            }

            if (isGrabbing)
            {
                DoGrab();
            }
        }
    }

    void FixedUpdate()
    {
    	if (!inPosition && tentaclesSpawned && !GameState.Paused)
    	{
	    	Vector3 pos = Vector3.Lerp(transform.position, SquidEndPosition, TimeAlive/SquidFadeInTime);
	    	transform.position = pos;
	    	inPosition = Mathf.Approximately(pos.x, SquidEndPosition.x) &&
	    			Mathf.Approximately(pos.y, SquidEndPosition.y);
    	}
    }

    protected void GenerateTentacles()
    {
        // For # of tentacles...
        GameObject prevSection = null;
        for (var i = 0; i < BasePositions.Length; i++)
        {
            prevSection = BasePositions[i];
            for (var j = 0; j < NumberOfSections; j++)
            {
                // Adjust position based on segment #
                Vector3 pos = BasePositions[i].transform.position;
                float yAdj = 0.15f + (0.2f * j);
                pos.y += yAdj;

                // Generate new object, set reasonable name
                GameObject section = (GameObject)Instantiate(TentacleSection, pos, Quaternion.identity);
                section.name = string.Format("Tentacle{0}Section{1}", i+1, j+1);

                // Configure rigidbody on new section
                Rigidbody2D sectionBody = section.GetComponent<Rigidbody2D>();
                sectionBody.mass = j == NumberOfSections - 1 ? LastSectionMass : MidSectionMass;
                sectionBody.drag = 2f;
                sectionBody.angularDrag = 0.25f;
                sectionBody.gravityScale = -1.0f;

                // Configure hinge joint between previous section and this one
                HingeJoint2D joint = prevSection.GetComponent<HingeJoint2D>();
                joint.autoConfigureConnectedAnchor = false;
                joint.connectedBody = sectionBody;
                joint.anchor = new Vector2(0f, j==0?0f:-0.15f);
                joint.connectedAnchor = new Vector2(0f, 0.15f);

                prevSection = section;
            }

            // Disable the last joint, so that the object may move freely
            prevSection.GetComponent<HingeJoint2D>().enabled = false;

            // Enable the last collider
            prevSection.GetComponent<BoxCollider2D>().enabled = true;

            // Store the last section in EndSections
            EndSections[i] = prevSection;
            isAlive[i] = true;
        }

       tentaclesSpawned = true;
    }

    protected void HandleInput()
    {
        if (!isGrabbing && !GameState.Paused)
        {
            for (var i = 0; i < EndSections.Length; i++)
            {
            	if (isAlive[i])
            	{
	                string axis = "Tentacle " + (i+1);
	                if (Input.GetButton(axis + " left")) {
	                    MoveTentacle(i, -1);
	                } else if (Input.GetButton(axis + " right")) {
	                    MoveTentacle(i, 1);
	                }
            	}
            }
        }
        if (Input.GetButton("Grab"))
        {
        	DiveTentacles();
        }
        if (Input.GetButton("Cancel"))
        {
            GameState.Paused = true;
        }
    }

    protected void DoGrab()
    {
        // @TODO: Do grab
    }

    protected void MoveTentacle(int idx, float dir)
    {
        GameObject t = EndSections[idx];
        Vector2 pos = t.transform.position;

        // apply movement
        Vector2 f = new Vector2(1.0f, 0.5f);
        f.x = dir * 1.0f;
        f.Normalize();
        pos += f*TentacleMoveSpeed;

        // clamp position within the screen
        pos.x = Mathf.Clamp(pos.x, orthoBounds.min.x, orthoBounds.max.x);
        pos.y = Mathf.Clamp(pos.y, orthoBounds.min.y, orthoBounds.max.y);
        t.transform.position = pos;
    }

    protected void DiveTentacles()
    {
    	for (var i = 0; i < EndSections.Length; i++)
    	{
    		if (isAlive[i])
    		{
		        GameObject t = EndSections[i];
		        Vector2 pos = t.transform.position;
    			Vector2 f = Vector2.down;

		        f.Normalize();
		        pos += f*TentacleMoveSpeed;
		        pos.y = Mathf.Clamp(pos.y, orthoBounds.min.y, orthoBounds.max.y);
		        t.transform.position = pos;
    		}
    	}
    }

    protected void Die()
    {
        PauseMenu.SetActive(true);
        transform.position = SquidStartPosition;
        BarrelContainer.SendMessage("DestroyBarrels");
        BoatContainer.SendMessage("DestroyBoats");
        GameState.Paused = true;
    }

    public void Score()
    {
        score += 1;
        if (score > GameState.HighScore)
        {
            GameState.HighScore = score;
        }
        GameState.Score = score;
        ScoreUI.text = score.ToString();
    }

    public void UnPause()
    {
        GameState.Paused = false;
        if (AllDead())
        {
        	Reset();
        }
    }

    public void DestroyTentacle(GameObject tentacle)
    {
    	Match m = Regex.Match(tentacle.name, @"\d");
		if (m.Success) {
	    	int id = int.Parse(m.Value);
	    	for (var i = 1; i <= NumberOfSections; i++)
	    	{
	    		isAlive[(id-1)] = false;
	    		string name = string.Format("Tentacle{0}Section{1}", id, i);
	    		GameObject section = GameObject.Find(name);
	    		if (section)
	    		{
	    			Destroy(section.gameObject);
	    		}
	    	}
		}
		if (AllDead()) {Die();}
    }

    protected bool AllDead()
    {
		bool anyAlive = false;
		for (var i = 0; i < isAlive.Length; i++)
		{
			if (isAlive[i]) {anyAlive = true;}
		}
		return !anyAlive;
    }

    protected void Reset()
    {
    	inPosition = false;
    	tentaclesSpawned = false;

        TimeAlive = 0;
        EndSections = new GameObject[4];
        isAlive = new bool[4];
        GenerateTentacles();
    }
}
