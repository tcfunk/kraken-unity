﻿using UnityEngine;
using System.Collections;

public class RepoBoat : MonoBehaviour {

    public float waveFreq = 1.0f;
    public float moveSpeed = 10.0f;

    protected float baseY;
    protected float age;
    protected SpriteRenderer leftSail;

    // Camera info
    Bounds orthoBounds;

    bool isDead = false;

    // Use this for initialization
    void Start () {
        float x = Camera.main.orthographicSize;
        float y = x / Camera.main.aspect;
        orthoBounds = new Bounds(Vector3.zero, new Vector3(x*4, y*4, 0f));

        age = 0f;
        baseY = transform.position.y;
        leftSail = (SpriteRenderer)GetComponentInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameState.Paused)
        {
            UpdateAge();
            UpdatePosition();
            UpdateRotation();
            RemoveIfOffStage();
        }
    }

    protected void UpdateAge() {
        age += Time.deltaTime;
    }

    protected void UpdatePosition() {
        if (!isDead)
        {
            float dy = Mathf.Cos(age*Mathf.PI)*0.25f;
            Vector3 pos = transform.position;
            pos.y = baseY + dy;

            pos.x += moveSpeed * Time.deltaTime;
            transform.position = pos;
        }
    }

    protected void UpdateRotation() {

    }

    protected void RemoveIfOffStage() {
        Vector3 pos = leftSail.bounds.min;
        if (pos.x > orthoBounds.max.x || pos.y < orthoBounds.min.y) {
            Destroy(this.gameObject);
        }
    }

    public void SetMoveSpeed(float speed) {
        moveSpeed = speed;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
    	string colliderName = collision.gameObject.name;
        if (colliderName.Contains("Tentacle"))
        {
            isDead = true;
        } else if (colliderName.Contains("Kraken")) {
        	GameObject.Find("Kraken").SendMessage("Score");
        	Destroy(this.gameObject);
        }
    }
}
