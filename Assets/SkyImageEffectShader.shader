﻿Shader "Custom/SkyImageEffectShader"
{
	Properties
	{
    _Color1 ("Main Color", Color) = (1,1,1,0.5)
    _Color2 ("Secondary Color", Color) = (1,1,1,0.5)
    _MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			fixed4 _Color1;
			fixed4 _Color2;

			fixed4 frag (v2f i) : SV_Target
			{
				float2 vu = i.uv;
				vu.y = 1 - vu.y;
//				vu.x = vu.y;
//				vu.y = vu.x;
//				float a = length(vu);
//				a = floor(a*10)/10;
				float a = vu.y;
				if (a >= 0.8) {
					a = 0.8;
				} else if (a >= 0.6) {
					a = 0.6;
				} else if (a >= 0.4) {
					a = 0.4;
				} else if (a >= 0.2) {
					a = 0.2;
				} else {
					a = 0.0;
				}

				return (1-a)*_Color1 + a*_Color2;
			}
			ENDCG
		}
	}
}
