﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class Repo {

	public int id;
	public RepoOwner owner;
	public string name;
	public string full_name;
	public string description;
	public bool _private;
	public bool fork;
	public string url;
	public string html_url;


	public static Repo CreateFromJSON(string jsonString) {
		return JsonUtility.FromJson<Repo>(jsonString);
	}
}
