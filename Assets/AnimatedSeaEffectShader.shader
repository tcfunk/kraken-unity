﻿Shader "Custom/AnimatedSeaEffectShader"
{
	Properties
	{
      _ReverseOscillation ("Reverse Oscillation", Float) = 1
      _Color1 ("Main Color", Color) = (1,1,1,0.5)
      _MainTex ("Texture", 2D) = "white" {}
  }
  SubShader
	{
		// No culling or depth
		Cull Off ZWrite On ZTest Always

		Tags {
			"Queue" = "Transparent"
		}

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha // use alpha blending

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}

			static const float PI = 3.14159265f;
			static const float TWO_PI = 6.28318530717958;

			fixed4 _Color1;
			fixed4 _Color2;
      float _ReverseOscillation;

			fixed4 frag (v2f i) : SV_Target
			{
				float a = 1.0;
				float2 uv = i.uv;

				float freq = 6;
				float t = _Time.x*freq/2;
				float x = uv.x;

        // Oscillate left and right
				float dx = sin(t*PI) * _ReverseOscillation;

				float cutoff = sin((x - dx)*TWO_PI*freq)/freq + (1.0 - 1.0/freq);

				if (uv.y > cutoff) {
					a = 0.0;
				}

				_Color1.a = a;
				return _Color1;
			}
			ENDCG
		}
	}
}
